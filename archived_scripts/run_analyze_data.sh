#!/bin/bash
# To Run, "sbatch run_analyze_data.sh"

. /usr/local/anaconda3/bin/activate ./ds_practicum
if ([ $? -ne 0 ] && [ ! -d "./ds_practicum" ]); then # If conda activate failed and the ./ds_practicum directory does not exist
    echo Virtual env does not exist. Creating ds_practicum
    conda create --prefix ./ds_practicum python=3.7 > /dev/null 2>&1  || exit 1 # Create virtual env silently
    echo ds_practicum created
    . /usr/local/anaconda3/bin/activate ./ds_practicum
fi

pip install -r requirements.txt
python3 ./analyze_data.py

. /usr/local/anaconda3/bin/deactivate ./ds_practicum