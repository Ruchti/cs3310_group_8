from model import get_gan, get_discriminator, get_generator
from keras.engine.saving import load_model
from helpers import plot_group
from keras import backend as K
import numpy as np


ecg_length = 326
generator = load_model('./good_normal/generator_state')
real_data = './tight_bound_beats_new.npy'
output_dir = './test_set/'

def get_synthetic_data(n_samples):
   return generator.predict(np.random.random_sample((n_samples, ecg_length)))

def make_manual_testing_data(n_samples):
    indicies = np.array(range(n_samples))
    np.random.shuffle(indicies)
    synthetic_indexes = []
    for i in range(n_samples):
        if indicies[i] < (n_samples // 2):
            synthetic_indexes.append(i)
    synthetic_samples = get_synthetic_data(n_samples // 2)
    real_samples = np.load(real_data)
    np.random.shuffle(real_samples)
    real_samples = real_samples[:(n_samples // 2)]
    combined = np.concatenate((synthetic_samples, real_samples), axis=0)
    combined = combined[indicies]
    plot_group(combined, 'normal', dir=output_dir)
    print('Synthetic image numbers')
    print(synthetic_indexes)

make_testing_data(30)