from model_custom_layers import MinibatchDiscrimination
from keras.engine.saving import load_model
from keras.optimizers import Adam


def set_trainability(model, trainable=False):
    '''
    Freeze or unfreeze all of the weights in a given model
    '''
    model.trainable = trainable
    for layer in model.layers:
        layer.trainable = trainable


def save_models(logging_dir, generator, discriminator, gan):
    '''
    Store the states of all three required models in a given directory
    '''
    if generator:
        generator.save(logging_dir + "/generator_state")

    if discriminator:
        set_trainability(discriminator, False)
        discriminator.save(logging_dir + "/discriminator_state")

    if gan:
        gan.save(logging_dir + "/GAN_state")


def load_models(resume_from):
    '''
    Load the three models of a gan from a given directory. This only works with models saved by the save_models method.
    The 'resume_from' parameter shoudl be the same as 'logging_dir' passed to save_models
    '''
    generator = load_model('./outputs/{0}/generator_state'.format(resume_from))
    discriminator = load_model('./outputs/{0}/discriminator_state'.format(resume_from), custom_objects={'MinibatchDiscrimination': MinibatchDiscrimination})
    gan = load_model('./outputs/{0}/GAN_state'.format(resume_from), custom_objects={'MinibatchDiscrimination': MinibatchDiscrimination})
    generator.compile(loss='binary_crossentropy', optimizer=Adam(lr=1e-3))
    discriminator.compile(loss='binary_crossentropy', optimizer=Adam(lr=1e-3))
    gan.compile(loss='binary_crossentropy', optimizer=generator.optimizer)
    return generator, discriminator, gan