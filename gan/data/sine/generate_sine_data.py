import numpy as np
import random

training_samples = 10000
testing_samples = 3000
amplitude_range = (0.1, 0.9)
frequency_range = (1.0, 6.0) 
phase_range = (-np.pi, np.pi)

def get_data(n_samples):
    x = []
    for i in range(n_samples):
        amplitude = random.uniform(amplitude_range[0], amplitude_range[1])
        frequency = random.uniform(frequency_range[0], frequency_range[1])
        phase = random.uniform(phase_range[0], phase_range[1])
        sin = amplitude * np.sin(np.linspace(0, 2*np.pi*frequency, 250) + phase)
        x.append(sin)
    return np.array(x)

def save_data(data, name):
    np.save(name, data)

train = get_data(training_samples)
save_data(train, 'train')

test = get_data(testing_samples)
save_data(test, 'test')