from train_helpers import get_batches, plot_group, log, log_run, register_exception_hook
from model_helpers import load_models, set_trainability
from model import get_gan, get_discriminator, get_generator
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
from keras import backend as K
import numpy as np
import random
import time
import sys


# Setup logging
assert len(sys.argv) == 2
logging_dir = sys.argv[1]
register_exception_hook(logging_dir)


# GPU Setup
requires_GPU = True
assert not requires_GPU or len(K.tensorflow_backend._get_available_gpus()) > 0 


# Training Parameters
max_hours = 10
epochs = 1
batch_size = 64
label_smoothing = 0.0
logging_interval = 1
log(logging_dir, 'Batch Size: {0}; Label Smoothing: {1}; Epochs: {2}; Max Hours: {3}'.format(batch_size, label_smoothing, epochs, max_hours))


# Data info
ecg_length = 375# For original dataset 326 
training_data = './data/ecg/Afib_averages.npy'
log(logging_dir, 'ECG Length: {0}; Data Path: \"{1}\";'.format(ecg_length, training_data))


# Models
resume_from = '05-06-21|14:44:49' # Replace with log folder name for transfer learning model
if resume_from:
    log(logging_dir, 'Resuming training with transfer learning from model in {0}'.format(resume_from))
    generator, discriminator, gan = load_models(resume_from)
    log(logging_dir, 'Successfully loaded saved models')
else:
    generator = get_generator(ecg_length)
    discriminator = get_discriminator(ecg_length)
    gan = get_gan(generator, discriminator)


def generate_latent_noise(n_samples):
    return np.random.random_sample((n_samples, ecg_length))


def get_synthetic_data(n_samples):
    # Generate example waves. The input is noise and the latent space consists of only one value
    x = generator.predict(generate_latent_noise(n_samples))
    # A column of zeros representing that the the values are synthetic
    y = np.zeros((n_samples, 1)) + label_smoothing
    return x, y


def train():
    start = time.time()
    g_losses = []
    d_losses = []
    for epoch in range(epochs):
        d_loss = 0.0
        g_loss = 0.0
        for real_data_batch in get_batches(training_data, batch_size):
            # Set Up Data
            X_real, y_real = (real_data_batch, np.ones((len(real_data_batch), 1)) - label_smoothing)
            X_fake, y_fake = get_synthetic_data(batch_size)
            X, y = np.vstack((X_real, X_fake)), np.vstack((y_real, y_fake))

            # Train Discriminator
            set_trainability(discriminator, True)
            d_loss = discriminator.train_on_batch(X, y)
            set_trainability(discriminator, False)

            # Train model
            g_loss = gan.train_on_batch(generate_latent_noise(batch_size), np.ones((batch_size, 1)))
        # Logging
        if epoch % logging_interval == 0:
            d_losses.append(d_loss)
            g_losses.append(g_loss)
            log(logging_dir, 'Epoch #{0}:::Generative Loss: {1}; Discriminative Loss: {2}'.format(epoch, g_loss, d_loss))

        if (time.time() - start) >= (max_hours * 3600):
            log(logging_dir, "Max time reached. Exiting")
            break

    end = time.time()
    plot_group(get_synthetic_data(20)[0], 'generated waves', logging_dir)
    log_run(d_losses, g_losses, logging_dir, discriminator, generator, gan)
    log(logging_dir, "Completed in {0} minutes".format((end - start) / 60))


train()