# Sources
# https://arxiv.org/pdf/1606.03498.pdf
# https://github.com/fchollet/keras/pull/3677
# https://github.com/xulabs/aitom/blob/d5e2890d30ac56ea70c6a3e0b16c3e4c3fddcf61/aitom/model/generative/macromolecule/gan/models/iw_models.py
from keras.engine.topology import Layer
from keras import initializers
import keras.backend as K


def initNormal(shape, dtype=None):
    my_init = initializers.RandomNormal(mean=0., stddev=0.02, seed=None)
    return my_init(shape)


def initConstant(shape, dtype=None):
    my_init = initializers.Constant(value=0.0)
    return my_init(shape)


class MinibatchDiscrimination(Layer):
    def __init__(self, num_kernels, kernel_dim=16, **kwargs):
        super(MinibatchDiscrimination, self).__init__()
        self.num_kernels = num_kernels
        self.kernel_dim = kernel_dim
        super(MinibatchDiscrimination, self).__init__(**kwargs)


    def get_config(self):
        current_config = {'num_kernels': self.num_kernels, 'kernel_dim': self.kernel_dim}
        base_config = super(MinibatchDiscrimination, self).get_config()
        combined_config = dict(list(base_config.items()) + list(current_config.items()))
        return combined_config


    def build(self, input_shape):
        self.kernel = self.add_weight(name='kernel',
                                      shape=(self.num_kernels, input_shape[1], self.kernel_dim),
                                      initializer=initNormal,
                                      trainable=True)
        super(MinibatchDiscrimination, self).build(input_shape)


    def call(self, X):
        # This is a 3d Tensor of shape (num_batch_samples, num_filters, num_dims_in_hidden_space)
        # This is the list of M matricies in the paper https://arxiv.org/pdf/1606.03498.pdf
        Ms = K.reshape(K.dot(X, self.kernel), (-1, self.num_kernels, self.kernel_dim))

        # For each kernel(matrix row) in Ms, find the differnce between current row
        # and the corresponding row in every other M
        diffs = K.expand_dims(Ms, 3) - K.expand_dims(K.permute_dimensions(Ms, [1, 2, 0]), 0)
        abs_diffs = K.sum(K.abs(diffs), axis=2)
        minibatch_features = K.sum(K.exp(-abs_diffs), axis=2)
        return K.concatenate([X, minibatch_features], 1)


    def compute_output_shape(self, input_shape):
        return input_shape[0], input_shape[1] + self.num_kernels