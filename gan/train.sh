#!/bin/bash

################################################################################
#
# Bash script to run training on ROSIE with horovod
# To run on Rosie, run `sbatch ./train.sh` from the project home directory
# Ensure training data has been generated and is located in ./data/ (see generate_data.py)
#
################################################################################


# You _must_ specify the partition. Rosie's default is the 'teaching'
# partition for interactive nodes.  Another option is the 'batch' partition.
#SBATCH --partition=batch

# The number of nodes to request
#SBATCH --nodes=1

# The number of GPUs to request
#SBATCH --gpus=1

# The number of CPUs to request per GPU
#SBATCH --cpus-per-gpu=16

# Kill the job if it takes longer than the specified time
# format: <days>-<hours>:<minutes>
#SBATCH --output=/dev/null
# The above line prevents the standard, cluttered SLURM output file from being created.
# We use a custom logging system to capture our desired output instead. For difficult debugging tasks,
# it may be desireable to remove the above line and see the standard SLURM output


# Create logging directory
now=$(date +"%m-%d-%y|%H:%M:%S")
logdir="outputs/${now}" 
mkdir -p $logdir

# Path to container
container="/data/containers/msoe-tensorflow.sif"

# Command to run inside container
command="python ./train.py ${logdir}"

# Execute singularity container on node.
singularity exec --nv -B /data:/data ${container} /usr/local/bin/nvidia_entrypoint.sh ${command}
