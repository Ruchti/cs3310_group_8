from keras.layers import Dense, LSTM, Conv1D, Reshape, MaxPooling1D, Flatten, Bidirectional
from model_custom_layers import MinibatchDiscrimination
from keras.utils.vis_utils import plot_model
from model_helpers import set_trainability
from keras.models import Sequential
from keras.optimizers import Adam


def get_generator(ecg_length=250):
    model = Sequential()
    # The input shape of an LSTM is (batch_size, timesteps, values in time)
    # Batch size does not need to be defined here, but we only have one "feature" (voltage)
    # Therefore, we have to reshape signal to have a trailing dimension before training with this
    # configuration
    model.add(Reshape((ecg_length, 1), input_shape=(ecg_length,)))
    model.add(Bidirectional(LSTM(units=50, activation='tanh', return_sequences=True, input_shape=(ecg_length, 1))))
    model.add(Dense(1, activation='tanh'))
    model.add(Flatten())
    model.compile(loss='binary_crossentropy', optimizer=Adam(lr=1e-3))
    return model


def get_discriminator(ecg_length=250):
    model = Sequential()
    model.add(Reshape((ecg_length, 1), input_shape=(ecg_length,)))
    model.add(Conv1D(filters=1, kernel_size=3, input_shape=(10,)))
    model.add(MaxPooling1D(pool_size=2))
    model.add(Flatten())
    model.add(Dense(100, activation='relu'))
    model.add(MinibatchDiscrimination(num_kernels=1))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer=Adam(lr=1e-3))
    return model


def get_gan(generator, discriminator):
	set_trainability(discriminator, False)
	model = Sequential()
	model.add(generator)
	model.add(discriminator)
	model.compile(loss='binary_crossentropy', optimizer=generator.optimizer)
	return model
