from model_helpers import save_models
import matplotlib.pyplot as plt
import numpy as np
import traceback
import sys


def get_batches(numpy_file, batch_size=50):
    '''
    Generator functon that loads a batch of samples from a npy file without loading the entire thing
    into memory.
    '''
    mem_mapped = np.load(numpy_file, mmap_mode='r')
    for batch_num in range(len(mem_mapped) // batch_size):
        offset = batch_size * batch_num
        yield mem_mapped[offset:offset+batch_size]


def plot_group(samples, name, dir="./"):
    '''
    Plot a series of samples. Samples is a 2d list-like structure where each row is a single signal.
    '''
    i = 0
    for sample in samples:
        plt.plot(sample)
        plt.savefig(dir + '/{0}_{1}.png'.format(name, i))
        plt.clf()
        i+=1


def log(logging_dir, message):
    '''
    Print a string to a logging file called 'script_outputs.txt' in a dedicated logging folder
    '''
    f = open(logging_dir + "/script_outputs.txt", "a")
    f.write(str(message) + "\n")
    f.close()


def log_run(d_loss, g_loss, logging_dir, d=None, g=None, GAN=None):
    '''
    Perform any post-training logging including saving model state for trainsfer learning
    '''
    plt.plot(d_loss, label="Discriminitive Loss")
    plt.plot(g_loss, label="Generative Loss")
    plt.title("Discriminitive vs Generative Loss")
    plt.xlabel("Epoch")
    plt.legend()
    plt.savefig(logging_dir + "/loss_graph.png")
    save_models(logging_dir, g, d, GAN)


def register_exception_hook(logging_dir):
    '''
    Registers a handler to be called any time an uncaught exeption is thrown. Logs the exception message in 
    the custom output file
    '''
    def except_hook(type, value, tback):
        log(logging_dir, "\n\n-------EXCEPTION-------\n\n")
        log(logging_dir, str(value))
        log(logging_dir, traceback.format_tb(tback))
        sys.__excepthook__(type, value, tback) # Default exception handler
    sys.excepthook = except_hook
