# CS3310 ECG GAN
**Alexander Ruchti, Jack Flitcroft, William Busch, Michael Caballero**

This is the companion code for the paper written by CS3310 group eight. The code consists of two main sections

### ./gan
This directory contains all of the code necessarry to train a GAN on 1d data. It was used to generate sine and ECG waves. 
To run this code:
1. Ensure a npy file containing the desired training data is placed in `gan/data/<my data type>`. The npy file should contain a 2d numpy array. A python file that can be used to generate sine data is included in `gan/data/sine/`
2. Update `gan/train.py` with the desired batch size, epochs, etc. If GPU compute is necessary, change `requires_gpu` to true, and the script will fail if a GPU is not found
3. To run the training locally (not recommended) run the following commands from the repo's root directory<br/>
    `cd gan`<br/>
    `python train.py <my logging directory path>`<br/>
4. To run the training on Rosie, run the following commands from the repo's root.<br/>
    `cd gan`<br/>
    `sbatch train.sh`<br/>
    This will automatically create an output directory in `gen/outputs/<current data time>`. Training logs, example generated signals, and loss metrics will be created in this directory.

- Data/sine
	- Example spot for training model initially. Future data to train on will be stored in data/<datatype> folder 
- Generate_sine_data.py
	- Example file for creating sine waves that resemble the structure of the data we will be using in the 2d numpy format
- Model.py
	- Contains 3 methods for the GAN model structure
		- Get_generator(length of ecg) creates a generator for the GAN that uses one input feature (voltage) defining the activation function and loss function
		- Get_discriminator(length of ecg) creates a discriminator with the same feature and adds the appropriate layers to it. For now we have an input, convolution, maxpooling, flatten, dense, minibatchdiscrimination, and dense layer in that order
		- Get_gan(generator, discriminator) returns the gan model
- Model_custom_layers.py
	- Contains code for a minibatchDiscrimination layer. 
- Model_helpers.py
	- Contains helper methods for setting a model as trainable, saving, and loading previous models.
- Train.py
	- Sets parameters for training such as max time, batch size, epochs, etc.
	- Gets location of training data, and where to log
	- Executes training sequence
- Train.sh
	- Bash file for executing train.py on rosie shell
- Train_helpers.py
Basic helper methods for the train.py file for plotting, logging, and error handling


### ./data
- Data_Cleaning
	- 1_get_leads_from_set.py
		- This file takes the main dataset and visualizes some of the data using numpy, then splits off all of the data from a single lead based on the condition
	- 2_split_into_individual_beats.py
		- Using the splitting and similarity score metric, we split the given patient data into individual beats and take the good beats from that set and save into a numpy file
	- 3_split_into_individual_beats_afib.py
		- This file does the same thing as above but with afib. We use a different strategy for centering the data on the x-axis
	- 4_normalize_beats.py
		- Takt the output from either file 2 or 3 and center the beats so the average is along the x-axis at y=0
	- 5_generate_tight_beats.py
		- Create a smaller training set using only beats that are in a much tighter similarity score
- Archive
	- Hypothesis figures are dummy figures for the kind of data we hope to have
	- Splitting_test_file.py is used to hold temporary code that splits the beat strings into individual beats. This is a dev playground file
Splitting_data.py and analyze_data.py is used to gather quick information about the whole set, and for devs to play with the data or explore strategies for using it
